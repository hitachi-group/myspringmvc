package com.laptrinhjavaweb.controller.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EmployeeController {

    @Autowired
//    EmployeeMapperImpl mapper;


    @RequestMapping(value ="/nhan-vien")
    public String listEmployee() {
//        mapper.gelAllEmployees();
        return "list-employee";
    }
}

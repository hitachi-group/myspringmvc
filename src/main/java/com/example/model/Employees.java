package com.example.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Employees implements Serializable {
    private Integer EMPLOYEE_ID;

    private String FIRST_NAME;

    private String LAST_NAME;

    private String EMAIL;

    private String PHONE_NUMBER;

    private Date HIRE_DATE;

    private String JOB_ID;

    private BigDecimal SALARY;

    private BigDecimal COMMISSION_PCT;

    private Integer MANAGER_ID;

    private Short DEPARTMENT_ID;

    private static final long serialVersionUID = 1L;

    public Integer getEMPLOYEE_ID() {
        return EMPLOYEE_ID;
    }

    public void setEMPLOYEE_ID(Integer EMPLOYEE_ID) {
        this.EMPLOYEE_ID = EMPLOYEE_ID;
    }

    public String getFIRST_NAME() {
        return FIRST_NAME;
    }

    public void setFIRST_NAME(String FIRST_NAME) {
        this.FIRST_NAME = FIRST_NAME == null ? null : FIRST_NAME.trim();
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME == null ? null : LAST_NAME.trim();
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL == null ? null : EMAIL.trim();
    }

    public String getPHONE_NUMBER() {
        return PHONE_NUMBER;
    }

    public void setPHONE_NUMBER(String PHONE_NUMBER) {
        this.PHONE_NUMBER = PHONE_NUMBER == null ? null : PHONE_NUMBER.trim();
    }

    public Date getHIRE_DATE() {
        return HIRE_DATE;
    }

    public void setHIRE_DATE(Date HIRE_DATE) {
        this.HIRE_DATE = HIRE_DATE;
    }

    public String getJOB_ID() {
        return JOB_ID;
    }

    public void setJOB_ID(String JOB_ID) {
        this.JOB_ID = JOB_ID == null ? null : JOB_ID.trim();
    }

    public BigDecimal getSALARY() {
        return SALARY;
    }

    public void setSALARY(BigDecimal SALARY) {
        this.SALARY = SALARY;
    }

    public BigDecimal getCOMMISSION_PCT() {
        return COMMISSION_PCT;
    }

    public void setCOMMISSION_PCT(BigDecimal COMMISSION_PCT) {
        this.COMMISSION_PCT = COMMISSION_PCT;
    }

    public Integer getMANAGER_ID() {
        return MANAGER_ID;
    }

    public void setMANAGER_ID(Integer MANAGER_ID) {
        this.MANAGER_ID = MANAGER_ID;
    }

    public Short getDEPARTMENT_ID() {
        return DEPARTMENT_ID;
    }

    public void setDEPARTMENT_ID(Short DEPARTMENT_ID) {
        this.DEPARTMENT_ID = DEPARTMENT_ID;
    }

    @Override
    public String toString() {
        return "Employees [EMPLOYEE_ID=" + EMPLOYEE_ID + ", FIRST_NAME=" + FIRST_NAME + ", LAST_NAME=" + LAST_NAME
                + ", EMAIL=" + EMAIL + ", PHONE_NUMBER=" + PHONE_NUMBER + ", HIRE_DATE=" + HIRE_DATE + ", JOB_ID="
                + JOB_ID + ", SALARY=" + SALARY + ", COMMISSION_PCT=" + COMMISSION_PCT + ", MANAGER_ID=" + MANAGER_ID
                + ", DEPARTMENT_ID=" + DEPARTMENT_ID + "]";
    }


}